using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Self_Care_app_azorja.Data.Context;
using Self_Care_app_azorja.Helpers;
using Self_Care_app_azorja.Options;
using Self_Care_app_azorja.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Self_Care_app_azorja
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _databaseSettings = new DatabaseSettings();
            ConfigureDatabase();
            
        }

        public IConfiguration Configuration { get; }
        private DatabaseSettings _databaseSettings;


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            //Dodavanje mappera u Services/////////////////////////////////////////
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MyProfileAutoMapper());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            ///////////////////////////////////////////////////////////////////////

            services.AddDbContext<SelfCareAppDbContext>(opt => opt.UseNpgsql(_databaseSettings.ToString()));
            //Dodavanje IUserService, UserService, IAccountService, AccountService (dependency injection)
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IMinistrationService, MinistrationService>();
            services.AddTransient<IFaqService, FaqService>();
            services.AddControllers();

            //Dodavanje ActionFiltera i FluentValidation u Services/////////////////////////////////////////
            /*
            services.AddMvc(options =>
            {
                options.Filters.Add(new ValidationFilter());
            })
            .AddFluentValidation(options =>
            {
                options.RegisterValidatorsFromAssemblyContaining<Startup>();
            });
            */
            //Dodavanje ActionFiltera i FluentValidation u Services/////////////////////////////////////////


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Self_Care_app_azorja", Version = "v1" });
                //////////////////////////////////////////////////////////////////////////////////////////
                /////////////////////JWT support (authentication)/////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////
                /*var security = new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[0]}
                };
                x.AddSecurityDefinition("Bearer", new ApiKeyScheme){
                
                }*/

                /*
                services.AddAuthentication(option =>
                {
                    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;                    
                    option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;                    

                }).AddJwtBearer(options =>
                {
                    options.SaveToken = true;                                        
                    options.TokenValidationParameters = new TokenValidationParameters
                    {                        
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["JwtToken:Issuer"],
                        ValidAudience = Configuration["JwtToken:Issuer"],
                        RequireExpirationTime = false,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtToken:SecretKey"]))
                    };
                });
                */
                //////////////////////////////////////////////////////////////////////////////////////////
                /////////////////////JWT support (authentication)/////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////
            });
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Self_Care_app_azorja v1"));
            }
            // global cors policy
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true) // allow any origin
                .AllowCredentials()); // allow credentials

            //app.UseHttpsRedirection();
            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void ConfigureDatabase()
        {
            _databaseSettings.Server = Configuration.GetSection("ConnectionStrings:Server").Value;
            _databaseSettings.Port = Configuration.GetSection("ConnectionStrings:Port").Value;
            _databaseSettings.Database = Configuration.GetSection("ConnectionStrings:Database").Value;
            _databaseSettings.UserId = Configuration.GetSection("ConnectionStrings:UserId").Value;
            _databaseSettings.Password = Configuration.GetSection("ConnectionStrings:Password").Value;
        }
        //////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////JWT support (authentication)/////////////////////////////////////////
        /*public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {            
            var jwtSettings = new JwtSettings();
            configuration.Bind(nameof(jwtSettings), jwtSettings);            
            services.AddSingleton(jwtSettings);

            services.AddAuthentication(configureOptions: x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSettings.Secret)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    RequireExpirationTime = false,
                    ValidateLifetime = true
                };
            });

        }
        */
        ////////////////////////JWT support (authentication)/////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////
    }
}
