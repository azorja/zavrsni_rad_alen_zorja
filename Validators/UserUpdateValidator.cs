﻿using FluentValidation;
using Self_Care_app_azorja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Validators
{
    public class UserUpdateValidator : AbstractValidator<User>
    {
        public UserUpdateValidator()
        {
            RuleFor(user => user.Name)
                .NotEmpty()
                .Length(3, 30).WithMessage("Ime mora biti više od 3 a manje od 30 znakova duljine");
            RuleFor(user => user.Surname)
                .NotEmpty()
                .Length(3, 30).WithMessage("Prezime mora biti više od 3 a manje od 30 znakova duljine");
            RuleFor(user => user.OIB)
                .NotEmpty()
                .GreaterThan(9999999999).WithMessage("OIB mora biti 11 znamenaka")
                .LessThan(99999999999).WithMessage("OIB mora biti 11 znamenaka");
            RuleFor(user => user.EmailAddress)
                .NotEmpty()
                .Length(10, 50).WithMessage("Email adresa mora biti najmanje 10 znakova duga");                      
        }
    }
}
