﻿using FluentValidation;
using Self_Care_app_azorja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Validators
{
    public class MinistrationCreateValidator : AbstractValidator<Ministration>
    {
        public MinistrationCreateValidator()
        {
            RuleFor(x => x.MinistrationName).NotEmpty();
            RuleFor(x => x.ContractObligation).NotEmpty().LessThanOrEqualTo(5).GreaterThanOrEqualTo(1);
            RuleFor(x => x.AccountId).NotEmpty();
        }
    }
}
