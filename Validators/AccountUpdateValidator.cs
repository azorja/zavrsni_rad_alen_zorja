﻿using FluentValidation;
using Self_Care_app_azorja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Validators
{
    public class AccountUpdateValidator : AbstractValidator<Account>
    {
        public AccountUpdateValidator()
        {
            RuleFor(x => x.ConnectionAddress).NotEmpty();
            RuleFor(x => x.PreferredContactType).NotEmpty().LessThanOrEqualTo(4).GreaterThanOrEqualTo(1);
            RuleFor(x => x.TemporaryExpulsion).NotNull();
            RuleFor(x => x.DoNotCall).NotNull();
            RuleFor(x => x.PhoneNumber).NotEmpty().Length(8, 14);
        }
    }
}
