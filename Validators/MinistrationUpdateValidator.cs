﻿using FluentValidation;
using Self_Care_app_azorja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Validators
{
    public class MinistrationUpdateValidator : AbstractValidator<Ministration>
    {
        public MinistrationUpdateValidator()
        {
            RuleFor(x => x.MinistrationName).NotEmpty();
            RuleFor(x => x.ContractObligation).NotEmpty().LessThanOrEqualTo(5).GreaterThanOrEqualTo(1);
        }
    }
}
