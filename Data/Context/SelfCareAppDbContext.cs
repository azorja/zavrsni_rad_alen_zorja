﻿using Microsoft.EntityFrameworkCore;
using Self_Care_app_azorja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Data.Context
{
    public class SelfCareAppDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Ministration> Ministrations { get; set; }
        public DbSet<Faq> Faqs { get; set; }

        public SelfCareAppDbContext(DbContextOptions<SelfCareAppDbContext> options) : base(options) 
        {
            
        }

    }
}
