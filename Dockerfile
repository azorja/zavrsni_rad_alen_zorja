#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
RUN apt-get update && apt-get install
RUN apt-get install -y wget 
RUN apt-get install -y apt-transport-https
RUN wget https://packages.microsoft.com/config/ubuntu/20.10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN apt-get update
RUN apt-get install -y dotnet-sdk-5.0 
RUN dotnet tool install --global dotnet-ef
#RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
#RUN apt-get install -y nodejs
ENV PATH $PATH:/root/.dotnet/tools
WORKDIR /app
#EXPOSE 80


FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["Self_Care_app_azorja.csproj", "."]
RUN dotnet restore "Self_Care_app_azorja.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "Self_Care_app_azorja.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Self_Care_app_azorja.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Self_Care_app_azorja.dll"]
