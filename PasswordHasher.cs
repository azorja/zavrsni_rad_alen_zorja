﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Self_Care_app_azorja
{
    public static class PasswordHasher
    {
        private const int SaltSize = 16;
        private const int HashSize = 20;
        private const int DefaultIterations = 10000;
        private const string DefaultHashSignature = "$MOJ_HASH$V1$";

        public static string Hash(string password, int iterations)
        {
            // STEP 1 Create the salt value with a cryptographic PRNG:
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[SaltSize]);

            // STEP 2 Create the Rfc2898DeriveBytes and get the hash value:
            var PasswordBasedKeyDerivationFunction = new Rfc2898DeriveBytes(password, salt, iterations);
            var hash = PasswordBasedKeyDerivationFunction.GetBytes(HashSize);

            // STEP 3 Combine the salt and password bytes for later use:
            var hashBytes = new byte[SaltSize + HashSize];
            Array.Copy(salt, 0, hashBytes, 0, SaltSize);
            Array.Copy(hash, 0, hashBytes, SaltSize, HashSize);

            // Convert to base64
            var base64Hash = Convert.ToBase64String(hashBytes);

            // Format hash with extra information
            return string.Format("$MOJ_HASH$V1${0}${1}", iterations, base64Hash);
        }

        public static string Hash(string password)
        {
            return Hash(password, DefaultIterations);
        }

        public static bool IsHashSupported(string hashString)
        {
            return hashString.Contains(DefaultHashSignature);
        }

        // STEP 5 Verify the user-entered password against a stored password
        public static bool Verify(string password, string hashedPassword)
        {
            if (!IsHashSupported(hashedPassword))
            {
                throw new NotSupportedException("The hashtype is not supported");
            }

            // Extract iteration and base64 string
            var splittedHashString = hashedPassword.Replace(DefaultHashSignature, "").Split('$');
            var iterations = int.Parse(splittedHashString[0]);
            var base64Hash = splittedHashString[1];

            // Get hash bytes
            var hashBytes = Convert.FromBase64String(base64Hash);

            // Get salt
            var salt = new byte[SaltSize];
            Array.Copy(hashBytes, 0, salt, 0, SaltSize);

            // Create hash with given salt
            var PasswordBasedKeyDerivationFunction = new Rfc2898DeriveBytes(password, salt, iterations);
            byte[] hash = PasswordBasedKeyDerivationFunction.GetBytes(HashSize);

            // Get result

            for (var i = 0; i < HashSize; i++)
            {
                if (hashBytes[i + SaltSize] != hash[i])
                {
                    return false;
                }
            }

            return true;
        }
    }

}
