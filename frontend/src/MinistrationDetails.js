import { useState } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import useFetch from "./useFetch";
import { localhostUrl } from "./Url";

const MinistrationDetails = () => {
    const { id, accountId, userId } = useParams();
    const { data: ministration, error, isLoading } = useFetch(localhostUrl + '/api/Ministration/GetById?id=' + id);
    const history = useHistory();
    const [deleteInProgress, setDeleteInProgress] = useState(false);

    const handleClick = () => {
        setDeleteInProgress(true);
        fetch(localhostUrl + '/api/Ministration/Delete/', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(ministration)
        }).then(() => {
            console.log({ ministration });
            setDeleteInProgress(false);
            history.push('/users/' + userId + '/accounts/' + accountId)
        })
    }

    return (
        <div className="ministration-details">
            {isLoading && <div>Loading...</div>}
            {error && <div>{error}</div>}
            {ministration && (
                <article>
                    <h2>Opis računske usluge: </h2>
                    <p>Usluga: {ministration.ministrationName}</p>
                    <p>Ugovorna obveza: {ministration.contractObligation}</p>

                    <Link to={'/users/' + userId + '/accounts/' + accountId + '/ministrations/' + ministration.id + '/update'}>
                        <button id="updateButton">Update Ministration</button>
                    </Link>

                    {!deleteInProgress && <button id="deleteButton" onClick={handleClick}>Delete Ministration</button>}
                    {deleteInProgress && <button disabled>Deleting</button>}
                    <br />
                    <button id="cancelButton" onClick={() => { history.push('/users/' + userId + '/accounts/' + accountId) }}>Go Back</button>

                </article>
            )}
        </div>
    );
}

export default MinistrationDetails;