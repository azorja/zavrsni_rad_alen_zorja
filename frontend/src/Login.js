import { useState, useContext } from "react";
import { useHistory } from "react-router";
import { MyContext } from './App';
import { localhostUrl } from "./Url";

const Login = () => {
    const { handleLogin } = useContext(MyContext);
    const [emailAddress, setEmailAddress] = useState('');
    const [password, setPassword] = useState('');
    const history = useHistory();
    const [loginInProgress, setLoginInProgress] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (emailAddress !== "" && password !== "") {
            fetch(`${localhostUrl}/api/User/Login?emailAddress=${emailAddress}&password=${password}`)
                .then(res => {
                    if (res.ok) {
                        return res.json()
                    }
                    else {
                        throw Error('Could not fetch the user from that URL');
                    }
                })
                .then(user => {
                    console.log(user);
                    handleLogin({
                        id: user.id,
                        role: user.role,
                        name: user.name,
                        surname: user.surname
                    });
                    setLoginInProgress(false);
                    if (user.role === 'Admin') {
                        history.push('/home');
                    }
                    else {
                        history.push('/users/' + user.id);
                    }
                })
                .catch(err => {
                    if (err.name === 'AbortError') {
                        console.log('fetch aborted');
                    }
                    else {
                        setLoginInProgress(false);
                    }
                });
        }
    }

    return (
        <div className="login">
            <h2>Login User</h2>
            <form onSubmit={handleSubmit}>

                <label>Email:</label>
                <input type="text" required value={emailAddress} onChange={(e) => setEmailAddress(e.target.value)}></input>

                <label>Password:</label>
                <input type="password" required value={password} onChange={(e) => setPassword(e.target.value)}></input>

                {!loginInProgress && <button type="submit" id="loginButton">Login</button>}
                {loginInProgress && <button disabled>Logging</button>}
            </form>
        </div>

    );
}

export default Login;