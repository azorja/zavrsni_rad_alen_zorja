import { useContext } from "react";
import { Link } from "react-router-dom";
import { MyContext } from "./App";

const FaqList = ({ questions }) => {
    const { loginUser } = useContext(MyContext);

    const DisplayFaq = ({ faq }) => {
        return <>
            <h2>{faq.question}</h2>
            <p>{faq.answer}</p>
        </>;
    }

    return (
        <div className="faq-list">
            {questions.map((faqQuestion) => {
                let displayFaq = (loginUser.role === "Admin" || (loginUser.role === "User" && faqQuestion.answer)) ? true : false;

                return (displayFaq &&
                    <div className="faq-preview" key={faqQuestion.id}>
                        {loginUser.role === "Admin" && (
                            <Link to={'/faq/questions/' + faqQuestion.id}>
                                <DisplayFaq faq={faqQuestion} />
                            </Link>)
                        }

                        {loginUser.role === "User" && faqQuestion.answer && (
                            <div>
                                <DisplayFaq faq={faqQuestion} />
                            </div>
                        )}
                    </div>
                )
            })}
        </div>
    );
}

export default FaqList;