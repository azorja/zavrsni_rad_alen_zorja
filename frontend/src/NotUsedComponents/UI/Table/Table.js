import React, { useState, useEffect } from "react";
import { Table } from "rsuite";

 
const { Column, HeaderCell, Cell, Pagination } = Table;


const MyTable = ({ data, totalItems, page, pageSize, pageSizeMenu, onChangePage, onChangePageSize, columns, isLoading, onRowClick, ...props }) => {
    return (
        <>
            <Table data={data} autoHeight loading={isLoading} onRowClick={onRowClick}>
                {
                    columns && columns.map((o, i) => {
                        return <Column resizable={o.resizable} width={o.width}>
                                    <HeaderCell>{o.title}</HeaderCell>
                                    <Cell style={{cursor: "pointer"}} dataKey={o.key} />
                                </Column>
                        }
                    )
                }

            </Table>


            <Pagination
                lengthMenu={ pageSizeMenu || [
                    { value: 10, label: 10 },
                    { value: 20, label: 20 },
                    { value: 30, label: 30 },
                    { value: 40, label: 40 },
                    { value: 50, label: 50 }
                ]}
                activePage={page}
                displayLength={pageSize}
                total={totalItems}
                onChangePage={onChangePage}
                onChangeLength={onChangePageSize}
            />
        </>
    );
};

export default MyTable;