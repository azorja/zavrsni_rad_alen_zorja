import { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { MyContext } from "./App";


const Navbar = () => {
    const { loginUser, handleLogout } = useContext(MyContext);
    const history = useHistory();


    if (!loginUser) {
        history.push('/');
    }


    return (
        <nav className="navbar">
            <div><h1>Self Care App</h1> {loginUser && <p>User: {loginUser.name + ' ' + loginUser.surname}</p>}</div>

            <div className="links">
                {loginUser.role === 'Admin' && <Link to={'/home'}>Home</Link>}
                {loginUser.role === 'User' && <Link to={'/users/' + loginUser.id}>Home</Link>}
                {loginUser.role === 'Admin' && <Link to="/create">Create New User</Link>}
                {!loginUser && <Link to="/">Login</Link>}
                <Link to="/faq">Faq</Link>
                {loginUser && <Link onClick={handleLogout} to="/">Logout</Link>}

            </div>
        </nav>
    );
}

export default Navbar;