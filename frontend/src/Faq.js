import FaqList from "./FaqList";
import useFetch from "./useFetch";
import { localhostUrl } from "./Url";
import { Link } from "react-router-dom";
import { useContext } from "react";
import { MyContext } from "./App";

const Faq = () => {
    const { data: questions, isLoading, error } = useFetch(localhostUrl + '/api/Faq/GetAll');
    const { loginUser } = useContext(MyContext);


    return (
        <div className="faq-home">
            {loginUser.role==="Admin" && <Link to='/faq/create'>
                <button id="AddButton">Add Question and Answer</button>
            </Link>}
            {loginUser.role==="User" && <Link to='/faq/create'>
                <button id="AddButton">Add Question</button>
            </Link>}
            
            {error && <div>{error}</div>}
            {isLoading && <div>Loading....</div>}
            {questions && <FaqList questions={questions} />}
        </div>
    );
}

export default Faq;