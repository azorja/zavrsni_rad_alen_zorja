import { useState } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import useFetch from "./useFetch";
import { localhostUrl } from "./Url";

const AccountDetails = () => {
    const { id, userId } = useParams();
    const { data: account, error, isLoading } = useFetch( localhostUrl + '/api/Account/GetById?id=' + id);
    const history = useHistory();
    const [deleteInProgress, setDeleteInProgress] = useState(false);

    const handleClick = () => {
        setDeleteInProgress(true);
        fetch(localhostUrl + '/api/Account/Delete/', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(account)
        }).then(() => {
            console.log({ account });
            setDeleteInProgress(false);
            history.push('/users/' + userId);
            //history.push('/');//vracam se na Home Page
        })
    }

    return (
        <div className="account-details">
            {isLoading && <div>Loading...</div>}
            {error && <div>{error}</div>}
            {account && (
                <article>
                    <h2>Account Details:</h2>
                    <p>Broj telefona: {account.phoneNumber}</p>
                    <p>Adresa priključka: {account.connectionAddress}</p>
                    <p>Preferred Contact: {account.preferredContactType}</p>
                    <p>Temporary Expulsion: {!account.temporaryExpulsion && "false"} {account.temporaryExpulsion && "true"}</p>
                    <p>Do not call: {!account.doNotCall && "false"} {account.doNotCall && "true"}</p>
                    <h3>Ministrations activated:</h3>
                    {account.ministrations.map((ministration) => (
                        <div className="account-ministration-preview" key={ministration.id}>
                            <Link id="ministration-list" to={'/users/' + userId + '/accounts/' + id + '/ministrations/' + ministration.id}>
                                <h2>{ministration.ministrationName}</h2>
                            </Link>
                        </div>
                    ))}


                    <Link to={'/users/' + userId + '/accounts/' + account.id + '/ministration/create'}>
                        <button id="AddButton">Add Ministration</button>
                    </Link>
                    <Link to={'/users/' + userId + '/accounts/' + account.id + '/update'}>
                        <button id="updateButton">Update Account</button>
                    </Link>
                    {!deleteInProgress && <button id="deleteButton" onClick={handleClick}>Delete Account</button>}
                    {deleteInProgress && <button disabled>Deleting</button>}
                    <br />
                    <button id="cancelButton" onClick={() => { history.push('/users/' + userId) }}>Go Back</button>
                </article>
            )}
        </div>
    );
}

export default AccountDetails;