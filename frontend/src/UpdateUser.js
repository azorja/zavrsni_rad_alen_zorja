import { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { MyContext } from "./App";
import { localhostUrl } from "./Url";

const UpdateUser = () => {
    const { id } = useParams();
    const { listOfCreateErrors, setListOfCreateErrors, loginUser } = useContext(MyContext);
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [oib, setOib] = useState('');
    const [emailAddress, setEmailAddress] = useState('');
    const history = useHistory();
    const [updateInProgress, setUpdateInProgress] = useState(false);


    const handleSubmit = async (e) => {
        e.preventDefault();
        const user = { id, name, surname, oib, emailAddress };
        setUpdateInProgress(true);
        try {
            const res = await fetch(localhostUrl + '/api/User/UpdateNecessary?id=' + id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(user)
            })
            if (res.ok) {
                console.log({ user });
                setListOfCreateErrors(null);
                history.push('/users/' + id);//vracam se na Home Page
            }
            if (!res.ok) {
                const errors = await res.json();
                const errror = errors.map(err => err.errorMessage);
                console.log("dobio grešku:" + errror);
                setListOfCreateErrors(errror);
            }
        }
        catch (error) {
            console.error(error);
        }
        finally {
            setUpdateInProgress(false);
        }
    }

    useEffect(() => {
        const abortCont = new AbortController();
        if (id) {
            const fetchData = async () => {
                const response = await fetch(localhostUrl + '/api/User/GetById?id=' + id, { signal: abortCont.signal })
                const userData = await response.json();

                setName(userData.name);
                setSurname(userData.surname);
                setOib(userData.oib);
                setEmailAddress(userData.emailAddress);
            }
            fetchData();
        }
        return () => abortCont.abort();
    }, [id, listOfCreateErrors])

    return (
        <div className="update">
            <h2>Update User</h2>
            <form onSubmit={handleSubmit}>

                <label>User name:</label>
                <input type="text" required value={name} onChange={(e) => setName(e.target.value)}></input>

                <label>User surname:</label>
                <input type="text" required value={surname} onChange={(e) => setSurname(e.target.value)}></input>

                <label>OIB:</label>
                <input type="text" required value={oib} onChange={(e) => setOib(e.target.value)}></input>

                <label>Email:</label>
                <input type="text" required value={emailAddress} onChange={(e) => setEmailAddress(e.target.value)}></input>

                {!updateInProgress && loginUser.role==="Admin" && <button type="submit" id="updateButton">Update User</button>}
                {!updateInProgress && loginUser.role==="User" && <button type="submit" id="updateButton">Update My Data</button>}
                {updateInProgress && <button disabled>Updating user</button>}
            </form>
            <button type="button" id="cancelButton" onClick={() => { setListOfCreateErrors(); history.push('/users/' + id) }}>Cancel</button>
        </div>

    );
}

export default UpdateUser;