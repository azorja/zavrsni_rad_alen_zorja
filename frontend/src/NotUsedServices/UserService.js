/*import { usersUrl } from "../Config/ExpiredUrl";
import axios from "axios";
 
export const userService = {
  getUsers,
  getUser,
  createOrUpdateUser,
  deleteUser
};
 
async function getUsers(page, pageSize, userStatus) {
 
  return axios
    .get(usersUrl, { params: { page: page, take: pageSize, statusCode: userStatus } })
    .then((res) => res)
    .catch((err) => err.response);
 
}
 
async function getUser(id) {
  return axios
    .get(usersUrl + "/" + id)
    .then((res) => res)
    .catch((err) => err.response);
}
 
async function createOrUpdateUser(userId, data) {
  console.log("createOrUpdateUser userId", userId);
  console.log("createOrUpdateUser data", data);
 
  if (userId) {
    return axios
      .put(usersUrl, 
        {
          id: userId,
          title: data.title,
          html: data.content,
          countryCode: "HRV",
          statusCode: 1
        })
      .then((res) => res)
      .catch((err) => err.response);  
  }
 
  return axios
    .user(usersUrl, 
      {
        title: data.title,
        html: data.content,
        countryCode: "HRV",
        statusCode: 1
      })
    .then((res) => res)
    .catch((err) => err.response);
}
 
async function deleteUser(userId) {
  return axios
    .delete(usersUrl + "/" + userId)
    .then((res) => res)
    .catch((err) => err.response);
}
*/