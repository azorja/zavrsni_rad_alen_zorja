export const sortingService = {
    getSortingOptions
};
 
async function getSortingOptions() {
    return [
        { label: "Newest", value: "newest" },
        { label: "Oldest", value: "oldest" }
        // { label: t('Newest'), value: "newest" },
        // { label: t('Oldest'), value: "oldest" }
    ];
}