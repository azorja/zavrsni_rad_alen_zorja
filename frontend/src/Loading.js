const Loading = ({isLoading, buttonType, buttonId, buttonText, loadingText, onClickFunction}) => {


    return (
        <nav className="loading">
            <div className="loadingScreen">
                {!isLoading && <button type={buttonType} id={buttonId} onClick={onClickFunction} >{buttonText}</button>}
                {isLoading && <button disabled>{loadingText}</button>}
            </div>
        </nav>
    );
}

export default Loading;