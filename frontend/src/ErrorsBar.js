import { useContext, useEffect } from "react";
import { MyContext } from "./App";


const ErrorsBar = () => {
    const { listOfCreateErrors } = useContext(MyContext);


    useEffect(() => {
        const abortCont = new AbortController();
        //Dodati Abort controller iz useFetch hook-a    
        return () => abortCont.abort();
    }, [])




    return (
        <div>
            {listOfCreateErrors && listOfCreateErrors.length && <h3>Validation errors: </h3>}
            {listOfCreateErrors && listOfCreateErrors.length && listOfCreateErrors.map(err => (<ul key={err}>{err}</ul>))}

        </div>
    );
}

export default ErrorsBar;