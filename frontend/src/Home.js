import UserList from "./UserList";
import useFetch from "./useFetch";
import { localhostUrl } from "./Url";


const Home = () => {

    const { data: users, isLoading, error } = useFetch(localhostUrl + '/api/User/GetAll');


    return (
        <div className="home">
            {error && <div>{error}</div>}
            {isLoading && <div>Loading....</div>}
            {users && <UserList users={users} title="All users" />}
        </div>
    );
}

export default Home;