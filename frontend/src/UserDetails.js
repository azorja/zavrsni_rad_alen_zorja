import { useContext, useState } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import { MyContext } from "./App";
import useFetch from "./useFetch";
import { localhostUrl } from "./Url";

const UserDetails = () => {
    const { loginUser } = useContext(MyContext);
    const { id } = useParams();
    const { data: user, error, isLoading } = useFetch(localhostUrl + '/api/User/GetById?id=' + id);
    const history = useHistory();
    const [deleteInProgress, setDeleteInProgress] = useState(false);

    const handleClick = () => {
        setDeleteInProgress(true);
        fetch(localhostUrl + '/api/User/Delete/', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(user)
        }).then(() => {
            console.log({ user });
            //debugger;
            setDeleteInProgress(false);
            //history.go(-1);
            if (loginUser.role === "Admin") {
                history.push('/home');//vracam se na Home Page
            }
            else {
                history.push('/');//vracam se na Login Page
            }
        })
    }

    return (
        <div className="user-details">
            {isLoading && <div>Loading...</div>}
            {error && <div>{error}</div>}
            {user && (
                <article>

                    {loginUser.role === 'Admin' && <h2>Korisnik: {user.name}  {user.surname}</h2>}
                    {loginUser.role === 'Admin' && <h3>User details: </h3>}
                    {loginUser.role === 'User' && <h3 color="pink">My details: </h3>}
                    <p>OIB: {user.oib}</p>
                    <p>Email: {user.emailAddress}</p>
                    <p>Role: {user.role}</p>

                    <article id="accounts-article">
                        <h2>Accounts:</h2>
                    </article>


                    {user.accounts.map((account) => (
                        <div className="user-account-preview" key={account.id}>
                            <Link id="account-list" to={'/users/' + user.id + '/accounts/' + account.id}>
                                <h2>{account.phoneNumber + ', ' + account.connectionAddress}</h2>
                            </Link>
                        </div>
                    ))}
                        <Link to={'/users/' + user.id + '/account/create'}>
                            <button id="AddButton">Add Account</button>
                        </Link>

                        <Link to={'/users/' + user.id + '/update'}>
                            {loginUser.role === 'Admin' && <button id="updateButton">Update User</button>}
                            {loginUser.role === 'User' && <button id="updateButton">Update My Data</button>}
                        </Link>                        

                        {!deleteInProgress && loginUser.role === 'Admin' && <button id="deleteButton" onClick={handleClick}>Delete User</button>}
                        {!deleteInProgress && loginUser.role === 'User' && <button id="deleteButton" onClick={handleClick}>Delete Main Account {"(" + user.name + " " + user.surname + ")"}</button>}
                        {deleteInProgress && <button disabled>Deleting</button>}
                        <br />
                        {loginUser.role === 'Admin' && <button id="cancelButton" onClick={() => { history.push('/home') }}>Go Back</button>}
                </article>
            )}
        </div>
    );
}

export default UserDetails;