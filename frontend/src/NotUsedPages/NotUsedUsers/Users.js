import React, { useState, useEffect } from "react";

const Users = (props) => {
    let [isLoading, setIsLoading] = useState(false);
   
    useEffect(() => {
        setIsLoading(true)

   
        setTimeout(() => {
            setIsLoading(false)
            //isLoading = false
        }, 2000)

        
    }, []);


    return (   
        <>
        <h1 style={{color:'red', width:'100%'}}>Ovo je moj naslov</h1>
        <span>Ovo je neki tekst</span>   
        isLoading ? <span>Učitava se ekran..</span> : "Učitan"
        </>
    )
}


export default Users;