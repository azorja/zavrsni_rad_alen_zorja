import { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router";
import { useParams } from "react-router-dom";
import { MyContext } from "./App";
import { localhostUrl } from "./Url";


const CreateAccount = () => {
    const { id: userId } = useParams();
    const { listOfCreateErrors, setListOfCreateErrors } = useContext(MyContext);
    const [connectionAddress, setConnectionAddress] = useState('');
    const [preferredContactType, setPreferredContactType] = useState('1');
    const [temporaryExpulsion, setTemporaryExpulsion] = useState('false');
    const [doNotCall, setDoNotCall] = useState('false');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const history = useHistory();

    const handleSubmit = async (e) => {
        e.preventDefault();
        //debugger;

        const account = {
            connectionAddress, preferredContactType, temporaryExpulsion: temporaryExpulsion === "true" ? true : false,
            doNotCall: doNotCall === "true" ? true : false, phoneNumber, userId
        };

        setIsLoading(true);
        try {
            const res = await fetch(localhostUrl + '/api/Account/Create', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(account)
            })
            if (res.ok) {
                console.log("no errors" + { account });
                setListOfCreateErrors(null);
                history.push("/users/" + userId);//vracam se na User Details
            }
            if (!res.ok) {
                const errors = await res.json();
                const errror = errors.map(err => err.errorMessage);
                console.log("dobio grešku:" + errror);
                setListOfCreateErrors(errror);
            }
        }
        catch (error) {
            console.error(error);
        }
        finally {
            setIsLoading(false);
        }
    }

    useEffect(() => {
        const abortCont = new AbortController();
        //ciscenje s abort Controllerom
        //Dodan Abort controller iz useFetch hook-a                
        return () => abortCont.abort();
    }, [isLoading, listOfCreateErrors])


    return (
        <div className="create">
            <h2>Add a New Account</h2>
            <form onSubmit={handleSubmit}>

                <label>Account connectionAddress:</label>
                <input type="text" required value={connectionAddress} onChange={(e) => setConnectionAddress(e.target.value)}></input>

                <label>Account preferredContactType:</label>
                <select type="text" required value={preferredContactType} onChange={(e) => setPreferredContactType(e.target.value)}>
                    <option value="1">Mobitel</option>
                    <option value="2">Kućni telefon</option>
                    <option value="3">Email</option>
                    <option value="4">Pošta</option>
                </select>

                <label>temporaryExpulsion:</label>
                <select type="text" required value={temporaryExpulsion} onChange={(e) => setTemporaryExpulsion(e.target.value)}>
                    <option value="true">True</option>
                    <option value="false">False</option>
                </select>

                <label>doNotCall:</label>
                <select type="text" required value={doNotCall} onChange={(e) => setDoNotCall(e.target.value)}>
                    <option value="true">True</option>
                    <option value="false">False</option>
                </select>

                <label>phoneNumber:</label>
                <input type="text" required value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)}></input>

                {!isLoading && <button type="submit" id="AddButton">Add Account</button>}
                {isLoading && <button disabled>Adding account</button>}
                <button type="button" id="cancelButton" onClick={() => { setListOfCreateErrors(); history.push("/users/" + userId) }}>Go Back</button>
                <p>{connectionAddress} {phoneNumber}</p>
            </form>
        </div>
    );
}

export default CreateAccount;