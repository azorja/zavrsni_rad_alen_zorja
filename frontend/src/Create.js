import { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router";
import { MyContext } from './App';
import { localhostUrl } from "./Url";

const Create = () => {
    const { listOfCreateErrors, setListOfCreateErrors } = useContext(MyContext);
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [oib, setOib] = useState('');
    const [emailAddress, setEmailAddress] = useState('');
    const [role, setRole] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const history = useHistory();

    const handleSubmit = async (e) => {
        e.preventDefault();
        //debugger;
        const user = { name, surname, oib, emailAddress, role, password };
        setIsLoading(true);

        try {
            const res = await fetch(localhostUrl + '/api/User/Create', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(user)
            });
            if (res.ok) {
                console.log("no errors");
                console.log({ user });
                setListOfCreateErrors(null);
                //history.go(-1);
                history.push('/home');//vracam se na Home Page
            }
            if (!res.ok) {
                const errors = await res.json();
                const errror = errors.map(err => err.errorMessage);
                console.log("dobio grešku:" + errror);
                setListOfCreateErrors(errror);
            }
        }
        catch (error) {
            console.error(error);
        }
        finally {
            setIsLoading(false);

        }
    }


    useEffect(() => {
        const abortCont = new AbortController();
        //Dodan Abort controller iz useFetch hook-a    
        return () => abortCont.abort();
    }, [isLoading, listOfCreateErrors])



    return (
        <div className="create">
            <h2>Add a New User</h2>
            <form onSubmit={handleSubmit}>
                <label>User name:</label>
                <input type="text" required value={name} onChange={(e) => setName(e.target.value)}></input>
                <label>User surname:</label>
                <input type="text" required value={surname} onChange={(e) => setSurname(e.target.value)}></input>
                <label>OIB:</label>
                <input type="text" required value={oib} onChange={(e) => setOib(e.target.value)}></input>
                <label>Email:</label>
                <input type="text" required value={emailAddress} onChange={(e) => setEmailAddress(e.target.value)}></input>
                <label>Role:</label>
                <select type="text" required value={role} onChange={(e) => setRole(e.target.value)}>
                    <option value="Admin">Admin</option>
                    <option value="User">User</option>
                </select>
                <label>Password:</label>
                <input type="text" required value={password} onChange={(e) => setPassword(e.target.value)}></input>

                {!isLoading && <button type="submit" id="AddButton">Add User</button>}
                {isLoading && <button disabled>Adding user</button>}
                <button type="button" id="cancelButton" onClick={() => { setListOfCreateErrors(); history.go(-1) }}>Go Back</button>
                <p>{name} {surname}</p>
            </form>
        </div>
    );
}

export default Create;