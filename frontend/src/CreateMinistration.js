import { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router";
import { useParams } from "react-router-dom";
import { MyContext } from "./App";
import { localhostUrl } from "./Url";


const CreateMinistration = () => {
    const { id: accountId, userId } = useParams();
    const { listOfCreateErrors, setListOfCreateErrors } = useContext(MyContext);
    const [ministrationName, setMinistrationName] = useState('Internet');
    const [contractObligation, setContractObligation] = useState('1');
    const [isLoading, setIsLoading] = useState(false);
    const history = useHistory();

    const handleSubmit = async (e) => {
        e.preventDefault();
        //debugger;

        const ministration = { ministrationName, contractObligation, accountId };

        setIsLoading(true);
        try {
            const res = await fetch(localhostUrl + '/api/Ministration/Create', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(ministration)
            });
            if (res.ok) {
                setListOfCreateErrors(null);
                console.log("no errors" + { ministration });
                history.push('/users/' + userId + '/accounts/' + accountId);//vracam se na Home Page
            }
            if (!res.ok) {
                const errors = await res.json();
                const errror = errors.map(err => err.errorMessage);
                console.log("dobio grešku:" + errror);
                setListOfCreateErrors(errror);
            }
        }
        catch (error) {
            console.error(error);
        }
        finally {
            setIsLoading(false);
        }
    }

    useEffect(() => {
        const abortCont = new AbortController();
        return () => abortCont.abort();
    }, [isLoading, listOfCreateErrors])



    return (
        <div className="create">
            <h2>Add a New Ministration</h2>
            <form onSubmit={handleSubmit}>

                <label>MinistrationName:</label>
                <select type="text" required value={ministrationName} onChange={(e) => setMinistrationName(e.target.value)}>
                    <option value="Internet">Internet</option>
                    <option value="Mobitel">Mobitel</option>
                    <option value="Televizija">Televizija</option>
                </select>
                
                <label>ContractObligation</label>
                <select type="text" required value={contractObligation} onChange={(e) => setContractObligation(e.target.value)}>
                    <option value="1">Godina dana</option>
                    <option value="2">Dvije godine</option>
                    <option value="5">Pet godina</option>
                </select>

                {!isLoading && <button type="submit" id="AddButton">Add Ministration</button>}
                {isLoading && <button disabled>Adding account</button>}
                <button type="button" id="cancelButton" onClick={() => { setListOfCreateErrors(); history.push('/users/' + userId + '/accounts/' + accountId) }}>Go Back</button>
            </form>
        </div>
    );
}

export default CreateMinistration;