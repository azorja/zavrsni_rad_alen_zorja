import { useState } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import useFetch from "./useFetch";
import { localhostUrl } from "./Url";

const FaqDetails = () => {
    const { id } = useParams();
    const { data: faq, error, isLoading } = useFetch(localhostUrl + '/api/Faq/GetById?id=' + id);
    const history = useHistory();
    const [deleteInProgress, setDeleteInProgress] = useState(false);

    const handleClick = () => {
        setDeleteInProgress(true);
        fetch(localhostUrl + '/api/Faq/Delete/', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(faq)
        }).then(() => {
            console.log({ faq });
            setDeleteInProgress(false);
            history.push('/faq');
        })
    }

    return (
        <div className="faq-details">
            {isLoading && <div>Loading...</div>}
            {error && <div>{error}</div>}
            {faq && (
                <article>
                    <p>Pitanje: {faq.question}</p>
                    <p>Odgovor: {faq.answer}</p>

                    <Link to={'/faq/' + faq.id + '/update'}>
                        <button id="updateButton">Update faq</button>
                    </Link>

                    {!deleteInProgress && <button id="deleteButton" onClick={handleClick}>Delete Faq</button>}
                    {deleteInProgress && <button disabled>Deleting</button>}
                    <br />
                    <button id="cancelButton" onClick={() => { history.push('/faq') }}>Go Back</button>

                </article>
            )}
        </div>
    );
}

export default FaqDetails;