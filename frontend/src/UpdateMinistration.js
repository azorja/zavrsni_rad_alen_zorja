import { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { MyContext } from "./App";
import { localhostUrl } from "./Url";

const UpdateMinistration = () => {
    const { id, accountId, userId } = useParams();
    const { listOfCreateErrors, setListOfCreateErrors } = useContext(MyContext);
    const [ministrationName, setMinistrationName] = useState("");
    const [contractObligation, setContractObligation] = useState("");
    const history = useHistory();
    const [updateInProgress, setUpdateInProgress] = useState(false);

    useEffect(() => {
        const abortCont = new AbortController();
        if (id) {
            const fetchData = async () => {
                const response = await fetch(localhostUrl + '/api/Ministration/GetById?id=' + id, { signal: abortCont.signal })
                const ministrationData = await response.json();

                setMinistrationName(ministrationData.ministrationName);
                setContractObligation(ministrationData.contractObligation);
            }
            fetchData();
        }
        return () => abortCont.abort();
    }, [id, listOfCreateErrors])


    const handleSubmit = async (e) => {
        e.preventDefault();
        const updateMinistration = { id, ministrationName, contractObligation };
        setUpdateInProgress(true);

        try {
            const res = await fetch(localhostUrl + '/api/Ministration/UpdateNecessary?id=' + id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(updateMinistration)
            })
            if (res.ok) {
                setListOfCreateErrors(null);
                history.push('/users/' + userId + '/accounts/' + accountId + '/ministrations/' + id);
                setUpdateInProgress(false);
            }
            if (!res.ok) {
                const errors = await res.json();
                const errror = errors.map(err => err.errorMessage);
                console.log("dobio grešku:" + errror);
                setListOfCreateErrors(errror);
            }
        }
        catch (error) {
            console.error(error);
        }
        finally {
            setUpdateInProgress(false)
        }
    }



    return (
        <div className="update">
            <h2>Update Ministration</h2>
            <form onSubmit={handleSubmit}>

                <label>Ministration name: {ministrationName}</label>
                <select type="text" required value={contractObligation} onChange={(e) => setContractObligation(e.target.value)}>
                    <option value="1">Godina dana</option>
                    <option value="2">Dvije godine</option>
                    <option value="5">Pet godina</option>
                </select>

                {!updateInProgress && <button type="submit" id="updateButton">Update Ministration</button>}
                {updateInProgress && <button disabled>Updating ministration</button>}
                <p>{ministrationName} {contractObligation}</p>
            </form>
            <button type="button" id="cancelButton" onClick={() => { setListOfCreateErrors(); history.push('/users/' + userId + '/accounts/' + accountId + '/ministrations/' + id) }}>Cancel</button>
        </div>

    );
}

export default UpdateMinistration;