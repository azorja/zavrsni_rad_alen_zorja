import { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { MyContext } from "./App";
import { localhostUrl } from "./Url";


const UpdateAccount = () => {
    const { id, userId } = useParams();
    const { listOfCreateErrors, setListOfCreateErrors } = useContext(MyContext);
    const [connectionAddress, setConnectionAddress] = useState("");
    const [preferredContactType, setPreferredContactType] = useState("");
    const [temporaryExpulsion, setTemporaryExpulsion] = useState(false);
    const [doNotCall, setDoNotCall] = useState(false);
    const [phoneNumber, setPhoneNumber] = useState("");
    const history = useHistory();
    const [updateInProgress, setUpdateInProgress] = useState(false);

    useEffect(() => {
        const abortCont = new AbortController();
        //Dodan Abort controller kao u useFetch hook-u
        if (id) {
            const fetchData = async () => {
                const response = await fetch(localhostUrl + '/api/Account/GetById?id=' + id, { signal: abortCont.signal })                
                const accountData = await response.json();

                console.log("Got response from account:", accountData);
                setConnectionAddress(accountData.connectionAddress);
                setPreferredContactType(accountData.preferredContactType);
                setTemporaryExpulsion(accountData.temporaryExpulsion);
                setDoNotCall(accountData.doNotCall);
                setPhoneNumber(accountData.phoneNumber);
            }
            fetchData();
        }
        return () => abortCont.abort();
    }, [id, listOfCreateErrors])


    const handleSubmit = async (e) => {
        e.preventDefault();
        const updateAccount = {
            id, connectionAddress, preferredContactType, temporaryExpulsion: temporaryExpulsion === "true" ? true : false,
            doNotCall: doNotCall === "true" ? true : false, phoneNumber
        };
        setUpdateInProgress(true);

        console.log("Update my account:", updateAccount)

        try {
            const res = await fetch(localhostUrl + '/api/Account/UpdateNecessary?id=' + id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(updateAccount)
            })
            if (res.ok) {
                setListOfCreateErrors(null);
                history.push('/users/' + userId + '/accounts/' + id);//vracam se na prijašnju stranicu
            }
            if (!res.ok) {
                const errors = await res.json();
                const errror = errors.map(err => err.errorMessage);
                console.log("dobio grešku:" + errror);
                setListOfCreateErrors(errror);
            }
        }
        catch (error) {
            console.error(error);
        }
        finally {
            setUpdateInProgress(false)
        }
    }


    return (
        <div className="update">
            <h2>Update Account</h2>
            <form onSubmit={handleSubmit}>

                <label>Connection Address:</label>
                <input type="text" required value={connectionAddress} onChange={(e) => setConnectionAddress(e.target.value)}></input>

                <label>Preferred Contact Type:</label>
                <select type="text" required value={preferredContactType} onChange={(e) => setPreferredContactType(e.target.value)}>
                    <option value="1">Mobitel</option>
                    <option value="2">Kućni telefon</option>
                    <option value="3">Email</option>
                    <option value="4">Pošta</option>
                </select>

                <label>Temporary expulsion:</label>
                <select type="text" required value={temporaryExpulsion} onChange={(e) => setTemporaryExpulsion(e.target.value)}>
                    <option value="true">True</option>
                    <option value="false">False</option>
                </select>

                <label>Do not call:</label>
                <select type="text" required value={doNotCall} onChange={(e) => setDoNotCall(e.target.value)}>
                    <option value="true">True</option>
                    <option value="false">False</option>
                </select>

                <label>Phone Number:</label>
                <input type="text" required value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)}></input>

                {!updateInProgress && <button type="submit" id="updateButton">Update Account</button>}
                {updateInProgress && <button disabled>Updating account</button>}
                <p>{connectionAddress + " " + phoneNumber}</p>
            </form>
            <button type="button" id="cancelButton" onClick={() => { setListOfCreateErrors(); history.push('/users/' + userId + '/accounts/' + id) }}>Cancel</button>
        </div>

    );
}

export default UpdateAccount;