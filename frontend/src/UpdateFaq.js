import { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { MyContext } from "./App";
import { localhostUrl } from "./Url";

const UpdateFaq = () => {
    const { id } = useParams();
    const [question, setQuestion] = useState("");
    const [answer, setAnswer] = useState("");
    const { listOfCreateErrors, setListOfCreateErrors } = useContext(MyContext);
    const history = useHistory();
    const [updateInProgress, setUpdateInProgress] = useState(false);

    useEffect(() => {
        const abortCont = new AbortController();
        if (id) {
            const fetchData = async () => {
                const response = await fetch(localhostUrl + '/api/Faq/GetById?id=' + id, { signal: abortCont.signal })
                const faqData = await response.json();

                setQuestion(faqData.question);
                setAnswer(faqData.answer);
            }
            fetchData();
        }
        return () => abortCont.abort();
    }, [id])


    const handleSubmit = async (e) => {
        e.preventDefault();
        const updateFaq = { id, question, answer };
        setUpdateInProgress(true);

        try {
            const res = await fetch(localhostUrl + '/api/Faq/UpdateNecessary?id=' + id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(updateFaq)
            })
            if (res.ok) {
                setListOfCreateErrors(null);
                history.push('/faq');
                setUpdateInProgress(false);
            }
            if (!res.ok) {
                const errors = await res.json();
                const errror = errors.map(err => err.errorMessage);
                console.log("dobio grešku:" + errror);
                setListOfCreateErrors(errror);
                console.log(listOfCreateErrors);
            }
        }
        catch (error) {
            console.error(error);
        }
        finally {
            setUpdateInProgress(false)
        }
    }

    return (
        <div className="update">
            <h2>Update Faq</h2>
            <form onSubmit={handleSubmit}>

                <label>Question:</label>
                <input type="text" required value={question} onChange={(e) => setQuestion(e.target.value)}></input>

                <label>Answer:</label>
                <input type="text" required value={answer} onChange={(e) => setAnswer(e.target.value)}></input>

                {!updateInProgress && <button type="submit" id="updateButton">Update Question and answer</button>}
                {updateInProgress && <button disabled>Updating Question and Answer</button>}
            </form>
            <button type="button" id="cancelButton" onClick={() => { setListOfCreateErrors(); history.push('/faq') }}>Cancel</button>
        </div>

    );
}

export default UpdateFaq;