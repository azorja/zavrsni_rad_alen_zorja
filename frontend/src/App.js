import React, { useState } from 'react';
import Navbar from './Navbar';
import Home from './Home';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Create from './Create';
import CreateAccount from './CreateAccount';
import CreateMinistration from './CreateMinistration';
import UserDetails from './UserDetails';
import AccountDetails from './AccountDetails';
import MinistrationDetails from './MinistrationDetails';
import UpdateUser from './UpdateUser';
import UpdateAccount from './UpdateAccount';
import UpdateMinistration from './UpdateMinistration';
import NotFound from './NotFound';
import Faq from './Faq';
import Login from './Login';
import ErrorsBar from './ErrorsBar';
import FaqDetails from './FaqDetails';
import UpdateFaq from './UpdateFaq';
import CreateFaq from './CreateFaq';

export const MyContext = React.createContext();

export default function App() {

  const [loginUser, setLoginUser] = useState('');
  const [listOfCreateErrors, setListOfCreateErrors] = useState('');

  const handleLogin = (userData) => {
    console.log('handleLogin', userData);
    setLoginUser(userData);
  }
  const handleLogout = () => {
    console.log('Logout handled');
    setLoginUser('');
  }

  React.useEffect(() => {
    console.log('UseEffect: loginUser: ', loginUser);
  }, [loginUser]);

  return (
    <Router>
      <MyContext.Provider value={{ loginUser, handleLogin, handleLogout, listOfCreateErrors, setListOfCreateErrors }}>
        <div className="App">
          <Navbar />
          <ErrorsBar />
          <div className="content">
            <Switch>
              <Route exact path="/">
                <Login />
              </Route>
              <Route exact path="/home">
                <Home />
              </Route>
              <Route exact path="/create">
                <Create />
              </Route>
              <Route exact path="/users/:id/account/create">
                <CreateAccount />
              </Route>
              <Route exact path="/users/:userId/accounts/:id/ministration/create">
                <CreateMinistration />
              </Route>
              <Route exact path="/users/:id">
                <UserDetails />
              </Route>
              <Route exact path="/faq/questions/:id">
                <FaqDetails />
              </Route>              
              <Route exact path="/faq">
                <Faq />
              </Route>
              <Route exact path="/faq/:id/update">
                <UpdateFaq />
              </Route>
              <Route exact path="/faq/create">
                <CreateFaq />
              </Route>
              <Route exact path="/users/:userId/accounts/:id">
                <AccountDetails />
              </Route>
              <Route exact path="/users/:userId/accounts/:accountId/ministrations/:id">
                <MinistrationDetails />
              </Route>
              <Route exact path="/users/:id/update">
                <UpdateUser />
              </Route>
              <Route exact path="/users/:userId/accounts/:id/update">
                <UpdateAccount />
              </Route>
              <Route exact path="/users/:userId/accounts/:accountId/ministrations/:id/update">
                <UpdateMinistration />
              </Route>
              <Route exact path="*">
                <NotFound />
              </Route>
            </Switch>
          </div>
        </div>
      </MyContext.Provider>
    </Router>
  );
}
