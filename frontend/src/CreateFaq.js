import { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router";
import { MyContext } from "./App";
import { localhostUrl } from "./Url";


const CreateFaq = () => {
    const { listOfCreateErrors, setListOfCreateErrors, loginUser } = useContext(MyContext);
    const [question, setQuestion] = useState('');
    const [answer, setAnswer] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const history = useHistory();

    const handleSubmit = async (e) => {
        e.preventDefault();
        //debugger;

        const faq = { question, answer };

        setIsLoading(true);
        try {
            const res = await fetch(localhostUrl + '/api/Faq/Create', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(faq)
            });
            if (res.ok) {
                setListOfCreateErrors(null);
                console.log("no errors" + { faq });
                history.push('/faq');//vracam se na faq home
            }
            if (!res.ok) {
                const errors = await res.json();
                const errror = errors.map(err => err.errorMessage);
                console.log("dobio grešku:" + errror);
                setListOfCreateErrors(errror);
            }
        }
        catch (error) {
            console.error(error);
        }
        finally {
            setIsLoading(false);
        }
    }

    useEffect(() => {
        const abortCont = new AbortController();
        return () => abortCont.abort();
    }, [isLoading, listOfCreateErrors])



    return (
        <div className="create">
            <h2>Add a New Faq</h2>
            <form onSubmit={handleSubmit}>

                <label>Question:</label>
                <input type="text" required value={question} onChange={(e) => setQuestion(e.target.value)}></input>
                {loginUser.role === "Admin" && <div><label>Answer:</label>
                    <input type="text" required value={answer} onChange={(e) => setAnswer(e.target.value)}></input></div>}


                {!isLoading && loginUser.role === "Admin" && <button type="submit" id="AddButton">Add Question and Answer</button>}
                {!isLoading && loginUser.role === "User" && <button type="submit" id="AddButton">Add Question</button>}
                {isLoading && <button disabled>Adding Faq</button>}
                <button type="button" id="cancelButton" onClick={() => { setListOfCreateErrors(); history.push('/faq')}}>Go Back</button>
            </form>
        </div>
    );
}

export default CreateFaq;