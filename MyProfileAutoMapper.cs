﻿using AutoMapper;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja
{
    public class MyProfileAutoMapper : Profile
    {
        public MyProfileAutoMapper()
        {
            CreateMap<User, UserDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Surname, opt => opt.MapFrom(src => src.Surname))
                .ForMember(dest => dest.OIB, opt => opt.MapFrom(src => src.OIB))
                .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.EmailAddress))
                .ForMember(dest => dest.Accounts, opt => opt.MapFrom(src => src.Accounts));

            CreateMap<Account, AccountDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.ConnectionAddress, opt => opt.MapFrom(src => src.ConnectionAddress))
                .ForMember(dest => dest.PreferredContactType, opt => opt.MapFrom(src => src.PreferredContactType))
                .ForMember(dest => dest.TemporaryExpulsion, opt => opt.MapFrom(src => src.TemporaryExpulsion))
                .ForMember(dest => dest.DoNotCall, opt => opt.MapFrom(src => src.DoNotCall))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId));

            CreateMap<Ministration, MinistrationDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.MinistrationName, opt => opt.MapFrom(src => src.MinistrationName))
                .ForMember(dest => dest.ContractObligation, opt => opt.MapFrom(src => src.ContractObligation))
                .ForMember(dest => dest.AccountId, opt => opt.MapFrom(src => src.AccountId));

            CreateMap<Faq, FaqDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Question))
                .ForMember(dest => dest.Answer, opt => opt.MapFrom(src => src.Answer));
        }
    }
}
