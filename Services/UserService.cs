﻿using AutoMapper;
using FluentValidation.Results;
using Microsoft.EntityFrameworkCore;
using Self_Care_app_azorja.Data.Context;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using Self_Care_app_azorja.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Services
{
    public class UserService : IUserService
    {
        //private readonly UserCreateValidator validator = new UserCreateValidator();
        private readonly SelfCareAppDbContext _dbContext;
        private readonly IMapper _mapper;
        public UserService(SelfCareAppDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        //Get all users
        public IEnumerable<UserDto> GetAll()
        {
            var data = _dbContext.Users
                .Include(x => x.Accounts)
                    .ThenInclude(account => account.Ministrations)
                .ToList();
            
            var mappedResults = _mapper.Map<IEnumerable<UserDto>>(data);            
            return mappedResults;            
        }

        //Get user by id
        public UserDto GetById(long id)
        {            
            var data=_dbContext.Users
                .Where(x => x.Id == id) //gdje je Id == id
                .Include(x => x.Accounts) //includam accounte vezane uz Usera                
                    .ThenInclude(account => account.Ministrations)
                .ToList()
                .SingleOrDefault(); //vraca jedini ili defaultni podatak, baca exception ako ima vise od jednog istog podatka

            var mappedResults = _mapper.Map<UserDto>(data);
            return mappedResults;            
        }
        //UserDto GetByEmail(string email);

        public UserDto GetByEmailAddress(string emailAddress)
        {
            var data = _dbContext.Users
                .Where(x => x.EmailAddress == emailAddress)
                .Include(x => x.Accounts)
                .ThenInclude(account => account.Ministrations)
                .ToList()
                .SingleOrDefault();

            var mappedResults = _mapper.Map<UserDto>(data);
            return mappedResults;
        }

        //Create a new user
        public void Create(User user)
        {
            var hashedPassword = PasswordHasher.Hash(user.Password);
            user.Password = hashedPassword;
            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();
            /*
            ValidationResult result = validator.Validate(user);
            if (result.IsValid)
            {
                var hashedPassword = PasswordHasher.Hash(user.Password);
                user.Password = hashedPassword;
                _dbContext.Users.Add(user);
                _dbContext.SaveChanges();
            }
            if (!result.IsValid)
            {
                foreach (var failure in result.Errors)
                {
                    Console.WriteLine("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                }
            }
            */
        }
        //Update user data
        public void Update(User user)
        {
            _dbContext.Users.Update(user);
            _dbContext.SaveChanges();
        }
        public void UpdateNecessary(User user)
        {
            var userUpdate = _dbContext.Users.Where(u => u.Id == user.Id).First();
            userUpdate.Name = user.Name;
            userUpdate.Surname = user.Surname;
            userUpdate.OIB = user.OIB;
            userUpdate.EmailAddress = user.EmailAddress;
            _dbContext.SaveChanges();
        }
        //Delete user
        public void Delete(User user)
        {
            _dbContext.Users.Remove(user);
            _dbContext.SaveChanges();
        }

        //Login user
        public UserDto Login(string emailAddress, string password)
        {
            var user = _dbContext.Users
                .Where(x => x.EmailAddress == emailAddress)
                .SingleOrDefault();
            if(user!=null && PasswordHasher.Verify(password, user.Password))
            {
                var mappedResults = _mapper.Map<UserDto>(user);
                return mappedResults;
            }
            else
            {
                return null;
            }
        }

    }
}
