﻿using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Services
{
    public interface IFaqService
    {
        IEnumerable<FaqDto> GetAll();
        FaqDto GetById(long id);
        void Create(Faq faq);
        void UpdateNecessary(Faq faq);
        void Delete(Faq faq);
    }
}
