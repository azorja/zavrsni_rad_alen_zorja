﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Self_Care_app_azorja.Data.Context;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Services
{
    public class AccountService : IAccountService
    {
        private readonly SelfCareAppDbContext _dbContext;
        private readonly IMapper _mapper;

        public AccountService(SelfCareAppDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public IEnumerable<AccountDto> GetAll()
        {
            //return _dbContext.Accounts.ToList();
            var data = _dbContext.Accounts
                .Include(x => x.Ministrations)
                .ToList();

            var mappedResults = _mapper.Map<IEnumerable<AccountDto>>(data);
            return mappedResults;
        }
        public AccountDto GetById(long id)
        {
            //return _dbContext.Accounts.Find(id);
            var data = _dbContext.Accounts
                .Where(x => x.Id == id) //gdje je Id == id
                .Include(x => x.Ministrations) //includam Ministratione vezane uz Usera
                .ToList()
                .SingleOrDefault(); //vraca jedini ili defaultni podatak, baca exception ako ima vise od jednog istog podatka

            var mappedResults = _mapper.Map<AccountDto>(data);
            return mappedResults;
        }
        public void Create(Account account)
        {
            _dbContext.Accounts.Add(account);
            _dbContext.SaveChanges();
        }
        public void Update(Account account)
        {
            _dbContext.Accounts.Update(account);
            _dbContext.SaveChanges();
        }
        public void UpdateNecessary(Account account)
        {
            //Update samo potrebnih podataka ministracije
            var accountUpdate = _dbContext.Accounts.Where(a => a.Id == account.Id).First();            
            accountUpdate.PhoneNumber = account.PhoneNumber;                          //PhoneNumber
            accountUpdate.ConnectionAddress = account.ConnectionAddress;              //ConnectionAddress
            accountUpdate.PreferredContactType = account.PreferredContactType;        //PreferredContactType
            accountUpdate.TemporaryExpulsion = account.TemporaryExpulsion;            //TemporaryExpulsion
            accountUpdate.DoNotCall = account.DoNotCall;                              //DoNotCall
            _dbContext.SaveChanges();                                                 //SaveChanges() in database
        }
        public void Delete(Account account)
        {
            _dbContext.Accounts.Remove(account);
            _dbContext.SaveChanges();
        }


    }
}
