﻿using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Services
{
    public interface IMinistrationService
    {
        IEnumerable<MinistrationDto> GetAll();
        MinistrationDto GetById(long id);
        void Create(Ministration ministration);
        void Update(Ministration ministration);
        void UpdateNecessary(Ministration ministration);
        void Delete(Ministration ministration);
    }
}
