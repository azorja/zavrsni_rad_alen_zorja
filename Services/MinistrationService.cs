﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Self_Care_app_azorja.Data.Context;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Services
{
    public class MinistrationService : IMinistrationService
    {
        private readonly SelfCareAppDbContext _dbContext;
        private readonly IMapper _mapper;
        public MinistrationService(SelfCareAppDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public IEnumerable<MinistrationDto> GetAll()
        {
            //return _dbContext.Ministrations.ToList();
            var data = _dbContext.Ministrations
                .ToList();
            var mappedResults = _mapper.Map<IEnumerable<MinistrationDto>>(data);
            return mappedResults;
        }
        public MinistrationDto GetById(long id)
        {
            //return _dbContext.Ministrations.Find(id);
            var data = _dbContext.Ministrations
                .Where(x => x.Id == id) //gdje je Id == id
                .ToList()
                .SingleOrDefault(); //vraca jedini ili defaultni podatak, baca exception ako ima vise od jednog istog podatka

            var mappedResults = _mapper.Map<MinistrationDto>(data);
            return mappedResults;           
        }
        public void Create(Ministration ministration)
        {
            _dbContext.Ministrations.Add(ministration);
            _dbContext.SaveChanges();
        }
        public void Update(Ministration ministration)
        {
            //var ministation = _dbContext.Ministrations.Where(m => m.Id == ministration.Id).First();
            //ministation.MinistrationName = ministration.MinistrationName;
            _dbContext.Update(ministration);
            _dbContext.SaveChanges();
        }
        public void UpdateNecessary(Ministration ministration)
        {
            //Update samo potrebnih podataka ministracije
            var ministrationUpdate = _dbContext.Ministrations.Where(m => m.Id == ministration.Id).First();
            ministrationUpdate.MinistrationName = ministration.MinistrationName;
            ministrationUpdate.ContractObligation = ministration.ContractObligation;
            _dbContext.SaveChanges();
        }
        public void Delete(Ministration ministration)
        {
            _dbContext.Ministrations.Remove(ministration);
            _dbContext.SaveChanges();
        }
    }
}
