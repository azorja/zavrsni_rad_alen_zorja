﻿using Self_Care_app_azorja.Data.Context;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Services
{
    public interface IUserService
    {
        IEnumerable<UserDto> GetAll();
        UserDto GetById(long id);
        UserDto GetByEmailAddress(string emailAddress);
        void Create(User user);
        void Update(User user);
        void UpdateNecessary(User user);
        void Delete(User user);
        UserDto Login(string emailAddress, string password);
    }
}
