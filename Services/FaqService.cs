﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Self_Care_app_azorja.Data.Context;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Services
{
    public class FaqService : IFaqService
    {

        private readonly SelfCareAppDbContext _dbContext;
        private readonly IMapper _mapper;

        public FaqService(SelfCareAppDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public IEnumerable<FaqDto> GetAll()
        {
            var data = _dbContext.Faqs
                .ToList();

            var mappedResults = _mapper.Map<IEnumerable<FaqDto>>(data);
            return mappedResults;
        }
        public FaqDto GetById(long id)
        {
            //return _dbContext.Accounts.Find(id);
            var data = _dbContext.Faqs
                .Where(x => x.Id == id) //gdje je Id == id               
                .ToList()
                .SingleOrDefault(); //vraca jedini ili defaultni podatak, baca exception ako ima vise od jednog istog podatka

            var mappedResults = _mapper.Map<FaqDto>(data);
            return mappedResults;
        }
        public void Create(Faq faq)
        {
            _dbContext.Faqs.Add(faq);
            _dbContext.SaveChanges();
        }
        public void UpdateNecessary(Faq faq)
        {
            //Update samo potrebnih podataka ministracije
            var faqUpdate = _dbContext.Faqs.Where(f => f.Id == faq.Id).First();
            faqUpdate.Question = faq.Question;          //Question
            faqUpdate.Answer = faq.Answer;              //Answer           
            _dbContext.SaveChanges();                   //SaveChanges() in database
        }
        public void Delete(Faq faq)
        {
            _dbContext.Faqs.Remove(faq);
            _dbContext.SaveChanges();
        }


    }
}
