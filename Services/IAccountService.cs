﻿using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Services
{
    public interface IAccountService
    {
        IEnumerable<AccountDto> GetAll();
        AccountDto GetById(long id);
        void Create(Account account);
        void Update(Account account);
        void UpdateNecessary(Account account);
        void Delete(Account account);
    }
}
