﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Models
{
    public class User
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }        
        [Required]
        public long OIB { get; set; }        
        [Required]        
        public string EmailAddress { get; set; }   
        public virtual List<Account> Accounts { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
    }
}
