﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Models.Dtos
{
    public class FaqDto
    {
        public long Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
