﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Models.Dtos
{
    public class AccountDto
    {
        public long Id { get; set; }
        public string ConnectionAddress { get; set; }
        public int PreferredContactType { get; set; }
        public bool TemporaryExpulsion { get; set; }
        public bool DoNotCall { get; set; }
        public string PhoneNumber { get; set; }
        public long UserId { get; set; }
        public virtual List<MinistrationDto> Ministrations { get; set; }
    }
}
