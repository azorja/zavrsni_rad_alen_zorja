﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Models.Dtos
{
    public class UserDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public long OIB { get; set; }
        public string EmailAddress { get; set; }
        public virtual List<AccountDto> Accounts { get; set; }
        public string Role { get; set; }        
    }
}
