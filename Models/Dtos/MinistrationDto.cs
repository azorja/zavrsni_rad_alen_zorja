﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Models.Dtos
{
    public class MinistrationDto
    {
        public long Id { get; set; }       
        public string MinistrationName { get; set; }
        public int ContractObligation { get; set; }
        public long AccountId { get; set; }
    }
}
