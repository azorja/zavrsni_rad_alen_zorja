﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Models
{
    public class Ministration
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string MinistrationName { get; set; }
        [Required]
        public int ContractObligation { get; set; }
        public long AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}
