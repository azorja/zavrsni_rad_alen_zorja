﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Models
{
    public class Account
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string ConnectionAddress { get; set; }     
        public int PreferredContactType { get; set; }        
        public bool TemporaryExpulsion { get; set; }
        public bool DoNotCall { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public long UserId { get; set; }
        public virtual User User { get; set; }
        public virtual List<Ministration> Ministrations { get; set; }
    }
}
