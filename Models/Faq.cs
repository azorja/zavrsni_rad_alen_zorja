﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Models
{
    public class Faq
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
