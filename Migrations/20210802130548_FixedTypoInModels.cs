﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Self_Care_app_azorja.Migrations
{
    public partial class FixedTypoInModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EmailAdress",
                table: "Users",
                newName: "EmailAddress");

            migrationBuilder.RenameColumn(
                name: "ConnectionAdress",
                table: "Accounts",
                newName: "ConnectionAddress");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EmailAddress",
                table: "Users",
                newName: "EmailAdress");

            migrationBuilder.RenameColumn(
                name: "ConnectionAddress",
                table: "Accounts",
                newName: "ConnectionAdress");
        }
    }
}
