﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Helpers
{
    public class DatabaseSettings
    {
        public string Server { get; set; }
        public string Port { get; set; }
        public string Database { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            return $"Server={Server};Port={Port};Database={Database};User Id={UserId};Password={Password};";
        }

    }
}
