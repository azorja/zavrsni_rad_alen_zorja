﻿//using Microsoft.AspNetCore.Components;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using Self_Care_app_azorja.Services;
using Self_Care_app_azorja.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet(nameof(GetAll))]
        public IEnumerable<AccountDto> GetAll()
        {
            return _accountService.GetAll();
        }
        [HttpGet(nameof(GetById))]
        public AccountDto GetById(long id)
        {
            return _accountService.GetById(id);
        }
        [HttpPost(nameof(Create))]
        public IActionResult Create(Account account)
        {
            AccountCreateValidator validator = new AccountCreateValidator();
            ValidationResult result = validator.Validate(account);
            if (result.IsValid)
            {
                _accountService.Create(account);
                return Ok();
            }
            return BadRequest(result.Errors);
        }
        [HttpPut(nameof(Update))]
        public void Update(Account account)
        {
            _accountService.Update(account);
        }
        [HttpPut(nameof(UpdateNecessary))]
        public IActionResult UpdateNecessary(Account account)
        {
            AccountUpdateValidator validator = new AccountUpdateValidator();
            ValidationResult result = validator.Validate(account);
            if (result.IsValid)
            {
                _accountService.UpdateNecessary(account);
                return Ok();
            }
            return BadRequest(result.Errors);
        }
        [HttpDelete(nameof(Delete))]
        public void Delete(Account account)
        {
            _accountService.Delete(account);
        }
    }
}
