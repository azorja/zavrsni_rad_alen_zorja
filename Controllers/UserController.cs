﻿//using Microsoft.AspNetCore.Components;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Self_Care_app_azorja.Data.Context;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using Self_Care_app_azorja.Services;
using Self_Care_app_azorja.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using RouteAttribute = Microsoft.AspNetCore.Components.RouteAttribute;

namespace Self_Care_app_azorja.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet(nameof(GetAll))]
        //[Authorize]
        public IEnumerable<UserDto> GetAll()
        {
            return _userService.GetAll();            
        }

        [HttpGet(nameof(GetById))]
        //[Authorize]
        public UserDto GetById(long id)
        {
            return _userService.GetById(id);
        }

        [HttpGet(nameof(GetByEmailAddress))]
        public UserDto GetByEmailAddress(string emailAddress)
        {
            return _userService.GetByEmailAddress(emailAddress);
        }

        [HttpPost(nameof(Create))]
        public IActionResult Create(User user)
        {
            UserCreateValidator validator = new UserCreateValidator();
            ValidationResult result = validator.Validate(user);
            if (result.IsValid)
            {
                ////////////////////////////////////////////////////result.Errors////////////////////////////////////////
                _userService.Create(user);
                return Ok();
            }
            return BadRequest(result.Errors);
        }

        [HttpPut(nameof(Update))]
        //[Authorize]
        public IActionResult Update(User user)
        {
            UserUpdateValidator validator = new UserUpdateValidator();
            ValidationResult result = validator.Validate(user);
            if (result.IsValid)
            {
                _userService.Update(user);
                return Ok();
            }
            return BadRequest(result.Errors);
        }

        [HttpPut(nameof(UpdateNecessary))]
        public IActionResult UpdateNecessary(User user)
        {
            UserUpdateValidator validator = new UserUpdateValidator();
            ValidationResult result = validator.Validate(user);
            if (result.IsValid)
            {
                _userService.UpdateNecessary(user);
                return Ok();
            }
            return BadRequest(result.Errors);
        }

        [HttpDelete(nameof(Delete))]
        //[Authorize]
        public IActionResult Delete(User user)
        {
            _userService.Delete(user);
            return Ok();
        }
        [HttpGet(nameof(Login))]
        public UserDto Login(string emailAddress, string password)
        {
            return _userService.Login(emailAddress, password);
            
        }
    }
}
