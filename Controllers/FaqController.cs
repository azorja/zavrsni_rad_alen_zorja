﻿//using Microsoft.AspNetCore.Components;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using Self_Care_app_azorja.Services;
using Self_Care_app_azorja.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaqController : ControllerBase
    {
        private readonly IFaqService _faqService;

        public FaqController(IFaqService faqService)
        {
            _faqService = faqService;
        }

        [HttpGet(nameof(GetAll))]
        public IEnumerable<FaqDto> GetAll()
        {
            return _faqService.GetAll();
        }
        [HttpGet(nameof(GetById))]
        public FaqDto GetById(long id)
        {
            return _faqService.GetById(id);
        }
        [HttpPost(nameof(Create))]
        public IActionResult Create(Faq faq)
        {
            _faqService.Create(faq);
            return Ok();
            /*FaqCreateValidator validator = new FaqCreateValidator();
            ValidationResult result = validator.Validate(faq);
            if (result.IsValid)
            {
                _faqService.Create(faq);
                return Ok();
            }
            return BadRequest(result.Errors);*/
        }        
        [HttpPut(nameof(UpdateNecessary))]
        public IActionResult UpdateNecessary(Faq faq)
        {
            _faqService.UpdateNecessary(faq);
            return Ok();
            /*FaqUpdateValidator validator = new FaqUpdateValidator();
            ValidationResult result = validator.Validate(faq);
            if (result.IsValid)
            {
                _faqService.UpdateNecessary(faq);
                return Ok();
            }
            return BadRequest(result.Errors);*/
        }
        [HttpDelete(nameof(Delete))]
        public void Delete(Faq faq)
        {
            _faqService.Delete(faq);
        }
    }
}
