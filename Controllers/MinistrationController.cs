﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Self_Care_app_azorja.Models;
using Self_Care_app_azorja.Models.Dtos;
using Self_Care_app_azorja.Services;
using Self_Care_app_azorja.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Self_Care_app_azorja.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MinistrationController : ControllerBase
    {
        private readonly IMinistrationService _ministrationService;
        public MinistrationController(IMinistrationService ministrationService)
        {
            _ministrationService = ministrationService;
        }
        [HttpGet(nameof(GetAll))]
        public IEnumerable<MinistrationDto> GetAll()
        {
            return _ministrationService.GetAll();
        }
        [HttpGet(nameof(GetById))]
        public MinistrationDto GetById(long id)
        {
            return _ministrationService.GetById(id);
        }
        [HttpPost(nameof(Create))]
        public IActionResult Create(Ministration ministration)
        {
            MinistrationCreateValidator validator = new MinistrationCreateValidator();
            ValidationResult result = validator.Validate(ministration);
            if (result.IsValid)
            {
                _ministrationService.Create(ministration);
                return Ok();
            }
            return BadRequest(result.Errors);
        }
        [HttpPut(nameof(Update))]
        public void Update(Ministration ministration)
        {
            _ministrationService.Update(ministration);
        }
        [HttpPut(nameof(UpdateNecessary))]
        public IActionResult UpdateNecessary(Ministration ministration)
        {
            MinistrationUpdateValidator validator = new MinistrationUpdateValidator();
            ValidationResult result = validator.Validate(ministration);
            if (result.IsValid)
            {
                _ministrationService.UpdateNecessary(ministration);
                return Ok();
            }
            return BadRequest(result.Errors);
        }
        [HttpDelete(nameof(Delete))]
        public void Delete(Ministration ministration)
        {
            _ministrationService.Delete(ministration);
        }
    }
}
